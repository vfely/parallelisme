#include <stdio.h>
#include <semaphore.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>

sem_t sem_thread;
sem_t sem_pizzaiolo[4];
pthread_t id_preparateur;
pthread_t id_thread[4];
int num_pizzaiolo[4];

void *travail_pizzaiolo(void *args)
{
	int num_pizzaiolo = *((int *) args);
	printf("Le pizzaiolo %d prepare sa pizza et se repose 4 secondes ---> \n", num_pizzaiolo);
	sem_post(&sem_thread);
	
	sleep(4); // se repose 4 secondes
	
	printf("<--- Fin du repos du pizzaiolo %d \n", num_pizzaiolo);
	sem_post(&sem_pizzaiolo[num_pizzaiolo]);
}

void *travail_preparateur(void *args)
{
	int ingredient1, ingredient2;
	int i;
	
	while (1) {
		do {
			// Génère deux nombres entre 1 et 3
			ingredient1 = random() % 3 + 1;
			ingredient2 = random() % 3 + 1;
		} while(ingredient1 == ingredient2);
		printf("\nLe preparateur dispose des ingrédients %d et %d \n", ingredient1, ingredient2);
		
		int j = 6 - ingredient1 - ingredient2; // pour déterminer le numéro du pizzaiolo qui sera appelé
		
		sem_wait(&sem_pizzaiolo[j]);
		pthread_create(&id_thread[j], NULL, travail_pizzaiolo,(void*)(&num_pizzaiolo[j]));
		
		sem_wait(&sem_thread); // attend que le pizzaiolo ait fini
	}
}

int main()
{
	int i;
	sem_init(&sem_thread, 0, 0);
	
	// Rend disponible les 3 pizzaiolos
	for (i = 1; i <= 3; i++) {
		sem_init(&sem_pizzaiolo[i], 0, 1);
		num_pizzaiolo[i] = i; // On assigne un numéro à chaque pizzaiolo
	}
	
	pthread_create(&id_preparateur, NULL, travail_preparateur, NULL); // On crée la thread du preparateur
	pthread_join(id_preparateur, NULL);
	
	return 0;
}
