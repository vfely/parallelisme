#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>

sem_t sem_thread;
sem_t sem_fin;
pthread_t id_thread_calcul[8];
pthread_t id_thread_saisie;
int nombre_decimal;
int num_thread = 1;

void *travail_thread_calcul(void *args)
{
	sem_wait(&sem_thread);
	
	printf("Travail de la thread %d : \n", num_thread);
	
	if(nombre_decimal % 2 == 0)
		printf("-> 0\n");
		
	else
		printf("-> 1\n");
		
	nombre_decimal = nombre_decimal / 2;
	
	if (nombre_decimal != 0) { // Si le nombre divisé n'est pas nul
		if (num_thread != 7) { // Si on est pas dans la dernière thread on peut en créer une autre
			num_thread++;
			pthread_create(&id_thread_calcul[num_thread], NULL, travail_thread_calcul, (void*)(&nombre_decimal));
		}
		
		else { // Sinon erreur
			printf("Erreur : Dépassement de la capacité du pipe-line \n");
			sem_post(&sem_fin);
			pthread_exit(NULL);
		}
	}
	
	else { // Si on a terminé les calculs en envoie le signal de fin
		sem_post(&sem_fin);
	}
	
	sem_post(&sem_thread);
}

void *travail_thread_saisie(void *args)
{
	printf("\nSaisir le nombre décimal à passer en base 2 : ");
	scanf("%d", &nombre_decimal);
	
	printf("Affichage de %d nombre en base 2 : \n", nombre_decimal);
	pthread_create(&id_thread_calcul[num_thread], NULL, travail_thread_calcul, (void*)(&nombre_decimal));
	
	sem_post(&sem_thread);
}

int main()
{
	// On initialise les semaphores et on crée la thread de saisie
	sem_init(&sem_thread, 0, 0);
	sem_init(&sem_fin, 0, 0);
	pthread_create(&id_thread_saisie, NULL, travail_thread_saisie, NULL);
	
	sem_wait (&sem_fin);
	return 0;
}
