#include <stdio.h>
#include <semaphore.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>

sem_t sem_thread;
pthread_t id_maman;
int nb_portions_init, nb_portions;

void *travail_maman(void *args)
{
	printf("Maman remplit le plat avec %i portions\n", nb_portions_init);
	nb_portions = nb_portions_init;
	
	sem_post(&sem_thread);
}

void *travail_oisillons(void *args)
{
	int num_oisillon = *((int *) args);
	printf("L'oiseau %d mange\n", num_oisillon);
	nb_portions--;
	printf("Il reste %d portions \n", nb_portions);
	
	if(nb_portions == 0)
	{
		printf("Le plat est vide --> On appelle la mère \n");
		pthread_create(&id_maman, NULL, travail_maman, (void*)(&nb_portions));
	}
	sleep(3); // chaque oisillon s'endort pour 3 secondes
	
	sem_post(&sem_thread);
}

int main()
{
	int nb_oisillons;
	printf("\nSaisir le nombre d'oisillons à nourrir : ");
	scanf("%d", &nb_oisillons);
	
	
	printf("\nSaisir le nombre de portions initiales : ");
	scanf("%d", &nb_portions_init);
	
	nb_portions = nb_portions_init; // On remplie l'assiette avec le nombre de portions initiales
	
	// On crée autant de variables et de threads qu'il y a d'oisillons
	int num_oisillon[nb_oisillons]; 
	pthread_t id_thread[nb_oisillons];
	
	sem_init(&sem_thread, 0, 0);

	int i;
	while(1) {
		for (i = 1; i <= nb_oisillons; i++) // On crée les threads pour tous les oisillons
		{
			num_oisillon[i] = i;
			pthread_create(&id_thread[i], NULL, travail_oisillons, (void*)(&num_oisillon[i]));
			sleep(1);
		}
		
		for(i = 0; i < nb_oisillons; i++) // Chaque oisillon attend que le précédent ait fini de manger
		{
			sem_wait(&sem_thread); 
		}
	}	
}
